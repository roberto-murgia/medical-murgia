+++
title = "PrivacyPolicy"
description = ""
keywords = [""]
+++



# Privacy Policy

##### Informazioni ai sensi dell’art. 7 D.L. n. 196 del 30.06.2003 in materia di “Tutela dei dati personali”

* I dati che Le sono richiesti verranno utilizzati da Medical Murgia SRL nel pieno rispetto di quanto previsto dal D.L. n. 196 del 30.06.2003 in materia di “Tutela dei dati personali”, per gli adempimenti connessi con l’attività dell’azienda (consultazione, utilizzo, elaborazione puntuale e statistica) a mezzo database interno.

* Ha il diritto, sotto la sua responsabilità, di non confidare alcuni dei dati richiesti, essendo però avvisato che in questo caso ciò potrebbe pregiudicare il rapporto in essere.

* In qualsiasi momento, secondo quanto previsto dall’art.7 del D.L. n. 196 del 30.06.2003, potrà consultare, modificare, o cancellare i Suoi dati scrivendo a: Medical Murgia S.r.l., C.so Duca degli Abruzzi, 2 – 10128 Torino

>
>
>
