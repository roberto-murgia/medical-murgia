+++
title = "Medicina Specialistica"
id = "MedicinaSpec"
+++

# Cosa offriamo 

Medical Murgia garantisce servizi di elevata qualità per il benessere dei propri pazienti. <br> <br>
Diamo molta importanza ad instaurare con il paziente un rapporto basato sulla <b>fiducia</b> attraverso 
la professionalità garantita di tutto il personale medico e tecnico-amministrativo che viene 
costantemente formato e aggiornato sulle nuove tecniche e tecnologie disponibili.  <br> <br>
La parte assistenziale, integrata da <b>personale infermieristico</b>, è regolarmente aggiornata sulle 
tecniche di assistenza medica e di <b>sterilizzazione</b>, elemento essenziale per un servizio professionale 
di alto livello e per la prevenzione delle infezioni crociate al pari di una sala operatoria ospedaliera. 