+++
title = "Chi siamo" 
description = "Frequently asked questions"
keywords = ["FAQ","How do I","questions","what if"]
+++


<h1>Medical Murgia Srl</h1>

Dalla pluriennale esperienza in campo medico e odontoiatrico del Dott. Murgia Marco nasce <b>Medical Murgia</b>.<br> 

Medical Murgia è un <b>Istituto Medico privato</b> specializzato nella diagnosi e nella terapia chirurgica delle patologie <b>odontoiatriche</b>, 
nella diagnosi clinica delle patologie <b>pediatriche</b> e <b>ortopediche</b>, <b>cardiache</b> e <b>dermatolgiche</b> e nella <b>medicina preventiva del lavoro</b>.<br>

<a href="/medical-murgia/04-equipe/"> Un'equipe selezionata </a> di medici opera nella sede torinese dell'Istituto, 
attrezzato con apparecchiature <b>laser</b> ed <b>elettromedicali</b> di ultima generazione.<br> <br> <br> 



<figure style="margin-bottom:20px; margin-right:0px; margin-left:50px; float:right; width:50%"/>
  <img src="../img/ChiSiamo/ChiSiamo1.png" style="margin-bottom:20px; width:100%"/>
  <figcaption align="middle"> </figcaption>
</figure>


<h2>LA TUA SALUTE E' UN DIRITTO</h2>
<h4>MIGLIORARE LA TUA QUALITA' DI VITA E' IL NOSTRO OBIETTIVO</h4>

Medical Murgia mira al miglioramento della tua qualità di vita offrendo prestazioni specialistiche che siano il frutto di <b>aggiornamento 
costante</b> e di <b>esperienza</b>, avvalendosi del supporto delle più moderne tecnologie. Inoltre, <b>disponibilità</b> e <b>cortesia</b> del personale 
sanitario e tecnico-amministrativo sono per noi indispensabili per offrirti un servizio completo. 

Il nostro intento è di accompagnarti nella tua personale ricerca del <b>benessere</b> fisico, psichico e sociale, 
agendo sia in un’ottica di prevenzione che di cura delle patologie. 



<br><br><br><br><br><br><br><br><br><br>
<figure style="margin-bottom:20px; margin-right:50px; margin-left:0px; float:left; width:50%"/>
 <!-- <a href="../img/carouselStudio/SalaSterile.JPG" target="_blank"><img src="../img/carouselStudio/SalaSterile.JPG" style="margin-bottom:20px; width:100%"/></a> -->
  <img src="../img/ChiSiamo/ChiSiamo2.png" style="margin-bottom:20px; width:100%"/>
  <figcaption align="middle"> </figcaption>
</figure>
 
<h2> LE BASI PER LA RICERCA DEL BENESSERE</h2>
<h4> UNA VISIONE COMPLETA PER CONOSCERSI MEGLIO</h4>

La nostra filosofia si basa su pochi, ma essenziali concetti: <br>
<ul>
<li>Ascolto</li>
<li>Visione globale del paziente</li>
<li>Approccio multidisciplinare</li>
<li>Cultura della salute e della prevenzione</li>
<li>Monitoraggio e supporto nel tempo</li>
</ul>

Ci impegniamo nella diffusione di un approccio alla salute fatto di <b>consapevolezza</b>, 
uno stile di vita sano nell'osservazione ed <b>ascolto costante di sè stessi</b>. 
<br><br><br><br><br><br><br><br><br><br>
<figure style="margin-bottom:20px; margin-right:0px; margin-left:50px; float:right; width:50%"/>
  <img src="../img/ChiSiamo/ChiSiamo3.png" style="margin-bottom:20px; width:100%"/>
  <figcaption align="middle"> </figcaption>
</figure>
<h2>PERCHE' SCEGLIERE MEDICAL MURGIA? </h2>

* Professionalità  ed esperienza: medici di comprovata esperienza e personale in costante aggiornamento. 
* Trasparenza: i nostri specialisti ti forniranno diagnosi chiare e discuteranno con te tutte le alternative terapeutiche in maniera precisa e dettagliata. 
* Rapporto diretto e su misura: curiamo nei dettagli il rapporto medico-paziente per offrirti un trattamento personalizzato e un'accoglienza sempre attenta e cortese. 
* Innovazione: le migliori tecnologie sul mercato e le ultime tecniche mediche saranno al tuo servizio. 



