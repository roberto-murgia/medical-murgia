+++ 
tipo = "Curriculum"
title = "Direttore Sanitario"
id = "Odontoiatira"
date = ""
mail = "marco.murgia@medicalmurgia.it"
tags = [""]
categories = [""]
banner = "img/banners/banner-4.jpg"
author = "Marco Murgia"
immagine = "img/StaffMM/foto1.jpg"
titolo1 = "Dott. Marco Murgia"
subtitolo = "Medico Chirurgo Odontoiatra"
telefono = "Tel. 011 331794"
+++


<h4> Breve CV </h4>


<b>Formazione</b>

Diploma di Qualifica professionale presso l'istituto I.P.S.I.A. G. Plana nel 1992.  
Diploma di Maturit� professionale presso l'istituto I.P.S.I.A. G. Plana nel 1993.  
Stage presso laboratorio odontoprotesico dal 1993 al 1994. Dal 1994 al 2002 studente a tempo pieno alla facolt� di Medicina e Chirurgia.  
Laurea a pieni voti in Medicina e Chirurgia presso l'Universit� di Torino nel 2002 e in Odontoiatria e Protesi Dentaria presso lo stesso ateneo nel 2007.  
Esame di stato e abilitazione professionale in Medicina e Chirurgia nel luglio 2003.  
Esame di stato e abilitazione professionale in Odontoiatria e Protesi Dentaria nel novembre 2007.


<b>Collaborazioni professionali</b>

Da settembre 2003 a maggio 2006 consulente presso ASL 8 sede di Moncalieri per la Medicina legale e sostituto di medicina generale presso due studi in Torino.  

Da gennaio 2004 a luglio 2007 prima medico prelevatore e poi medico accettatore per la FIDAS donatori di sangue. 

Collaboratore del reparto di Protesi della Dental School dal 2007 al 2011 come Tutor e come aiuto del Prof. G. Gassino per la Protesi Maxillo-Facciale. 



<b>Attività attuali</b>

Dal 2008 libero professionista e titolare di Studio Medico Dentistico in Torino. 

Dal 2011 ad oggi Responsabile e poi Direttore Sanitario dell'Osservatorio per la Salute Orale delle Comunit� Svantaggiate, ambulatorio del COI (Cooperazione Odontoiatrica Internazionale) ospite della Dental School, sede Lingotto. 

Collaboratore con l'Universit� di Odontoiatria di Torino per ricerca e innovazione scientifica in campo protesico.   


<b>Pubblicazioni</b>

Coautore del libro 
�La riabilitazione protesica nel paziente anziano� 
in collaborazione con l�Universit� degli Studi di Torino Dipartimento di Scienze Chirurgiche � C.I.R. Dental School -Edizioni A.L.E. - 2020 


<b>Affiliazioni</b> 

Iscritto dal 2008 ad ANDI (Associazione Nazionale Dentisti Italiani). 

<b>Comunicazioni a congressi come relatore</b> 

*	Protesi totale: evoluzioni innovative� M. Murgia � AIO �Giornate odontoiatriche valdostane� - Novembre 2016 
*	�Corsi in protesi rimovibile� M.Murgia - COI (Cooperazione Odontoiatrica Internazionale) - 2014/2015 

<b>Corsi e Congressi</b>

*	�L�implantologia post-estrattiva a carico immediato, sua evoluzione nel tempo� - Gruppo Ricerca Implantare � 2009 
*	V Simposio Nazionale Gruppo Ricerca Implantare � 2010 
*	Corso �I concentrati piastrinici a uso topico� - Azienda Ospedaliera Universitaria San Giovanni Bosco di Torino � 2011 
*	XIX Congresso Nazionale del Collegio dei Docenti di Odontoiatria - �L�high tech come supporto alla ricerca, alla didattica ed alla clinica in odontostomatologia� - Universit� degli Studi di Torino � 2012 
*	Corso �I Protocolli Operativi della Sterilizzazione� - 2012 
*	Corso �Le ricostruzioni Tridimensionali Ossee con Griglie in Titanio� Dott. F. Morelli � 2012 
*	Corso �Progettazione e metodo in protesi fissa� Dott. D. Baldi � 2012 
*	Corso �Implant site prep e piezochirurgia parodontale� Dott. R. Ceccarelli � 2012 
*	Corso �Ortodonzia e Piezochirurgia: un nuovo approccio al movimento ortodontico nell�adulto� Prof. M. Finotti � 2013 
*	Corso �Vantaggi in chirurgia ed implantologia attraverso l�utilizzo della tecnica piezoelettrica con particolare riferimento al Sinus Physiolift� ad allo split crest� Dott. R. Sentineri � 2013 
*	Corso annuale di chirurgia estetica parodontale avanzato con il Prof. G. Zucchelli � 2013/2014 
*	Corso �Diagnosi e Chirurgia parodontale� - 2014 
*	Corso Teorico: �BOPT: un nuovo approccio in protesi fissa� Dott. I. Loi � 2014 
*	Corso Teorico Pratico annuale presso l�Ospedale San Raffaele di Milano: �Post Graduate di Laser in odontoiatria� - 2015 
*	Master universitario di I livello con ECHITO �Salute Orale nelle comunit� svantaggiate e nei paesi a basso reddito� (VII edizione) - 2015/2016 
*	Corso �Protesi totale, aspetti estetici, funzionali e geriatrici� Prof. S. Palla, odt. D. Frigerio � ANTLO Odontotecnici Italiani � 2017 
*	Corso annuale �Un moderno approccio alla Parodontologia nello studio odontoiatrico: diagnosi, prognosi, terapia� Dott. R. Abundo, Dott. G. Corrente - 2018 
*	Congresso �10 Anni Dental School� Centro di eccellenza per l�assistenza, la didattica e la ricerca in campo odontostomatologico Prof. Carossa, Prof. Berutti � 2020 
