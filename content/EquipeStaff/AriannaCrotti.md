+++ 
tipo = "Curriculum"
title = "Arianna Crotti"
id = "Infermiera"
date = ""
mail = "arianna.crotti@medicalmurgia.it"
tags = [""]
categories = [""]
banner = "img/banners/banner-4.jpg"
author = "Arianna Crotti"
immagine = "img/Staff_Ext_MM/Medico2.png"
titolo1 = "Arianna Crotti"
subtitolo = "Infermiera"
telefono = "Tel. 011 331794"
+++



<h4> Breve CV </h4>


<b>Formazione</b>

Diploma di Maturità in Scienze Socio-Psicopedagogiche presso Liceo Regina Margherita nel 2011. 

Laurea triennale in Scienze Infermieristiche presso A.O.U. San Luigi Gonzaga nel 2015. 


<b>Collaborazioni professionali</b>

* Humanitas Gradenigo: infermiera di reparto nei reparti di Oncologia, Medicina Interna, Sala Prelievi, Covid19 + - dal 2016 ad oggi 
* Medical Service Assistance: prelievi domiciliari; prelievo di campioni biologici nell'Asl di competenza presso l'Ospedale Maria Vittoria – 2016 
* Assistenza domiciliare; pianificazione e assistenza domiciliare di paziente non cosciente in respiro non spontaneo; medicazione e gestione di tracheostomia; medicazione, gestione, somministrazione di PEG. - 2015 


<b>Attività  attuali</b>

* Humanitas Gradenigo: infermiera di reparto nei reparti di Oncologia, Medicina Interna, Sala Prelievi, Covid19 + 
* Collaborazione con Medical Murgia S.r.l.

<b>Corsi e Congressi</b>

* La gestione dell'incontinenza urinaria e fecale – 2016 
* L'evidence based practice nelle procedure infermieristiche – 2016 
* La sedazione terminale/palliativa: aspetti clinici ed etici – 2016 
* Gestione della mobilizzazione del paziente in ospedale: problemi e soluzioni pratiche – 2016 
* Gestione delle linee venose – 2016 
* Il sovrappeso e le sue complicanze nella donna – 2018 
* Approccio alle cure palliative in ambito respiratorio – 2018 
* La lettura critica dell'articolo medico-scientifico – 2018 
* Proteggere dall'influenza con la vaccinazione: un percorso teorico-pratico in 4 tappe per un efficace e consapevole proposta di salute – 2018 
* Coronaropatia e arteriopatia periferica: stato dell'arte – 2018 
* Vaccini e vaccinazioni: strategie e strumenti per la prevenzione delle malattie infettive – 2018 
* Educazione terapeutica: l'empowerment quale strategia di prevenzione del diabete e delle sue complicanze – 2018 
* Diagnostica di laboratorio: aggiornamento tecnico sul prelievo venoso – 2019 
* Tra assistenza e ricerca – 2019 
* Trattamento dell’atrofia vulvo-vaginale della post menopausa – 2019 
* Anemia sideropenica – 2019 
* Moderne acquisizioni in termini di autocontrollo glicemico, tecnica iniettiva, gestione in team della malattia diabetica, aspetti comportamentali – 2019 
* Compromissioni respiratorie e cardiovascolari del Covid-19: che cosa sappiamo? - 2020 
* La Covid-19 vista dalla medicina generale – 2020 
* Covid-19: guida pratica per operatori sanitari – 2020 
* Il sonno e i suoi disturbi. Una revisione degli aspetti fisiologici, clinici e di trattamento – 2020 
* La telemedicina e la cura a distanza ai tempi del covid-19 e del nostro futuro: la responsabilità del medico in telemedicina - 2020  
