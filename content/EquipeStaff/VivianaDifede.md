+++ 
tipo = "Curriculum"
title = "Viviana Di Fede"
id = "Igienista"
date = ""
mail = "viviana.difede@medicalmurgia.it"
tags = [""]
categories = [""]
banner = "img/banners/banner-4.jpg"
author = "Dott.ssa Viviana Di Fede"
immagine = "img/Staff_Ext_MM/Medico2.png"
titolo1 = "Dott.ssa Viviana Di Fede"
subtitolo = "Igienista"
telefono = "Tel. 011 331794"
+++ 


<h4> Breve CV </h4>



<b>Formazione</b>

Diploma di maturità e abilitazione odontotecnico conseguiti presso Istituto Giovanni Plana nel 2011. 
Laurea di primo livello in Igiene Dentale conseguita presso Dental School di Torino nel 2017. 
Tirocinio presso Dental School di Torino nel periodo 2014/2017. 

<b>Collaborazioni professionali</b>

Stage in laboratorio odontotecnico presso Odontomaster nel periodo 2008/2009. 
Assistente alla poltrona presso Dott. Corradi nel periodo 2011/2013. 

<b>AttivitÃ Â  attuali</b>

Da dicembre 2017 ad oggi Igienista Dentale presso studi dentistici privati. 

<b>Affiliazioni</b> 

Dal 2014 ad oggi socia ordinaria AIDI (Associazione Igienisti Dentali Italiana e SIDP (Società Italiana di Parodontologia e Implantologia). 

<b>Corsi e Congressi</b>

*	“La cura del paziente affetto da parodontite” - 2019 
*	“Vaccini e malattie prevenibili da vaccinazioni, basi immunologiche e nuovi approcci” - 2019 
*	“Proteggere dall'influenza con la vaccinazione: un percorso teorico-pratico in 4 tappe per un'efficace e consapevole proposta di salute.” - 2019 
*	“Conoscere e trattare il dolore in età pediatrica” - 2019 
*	“Emergenza sanitaria da nuovo coronavirus SARS CoV-2: preparazione e contrasto” - 2020 
*	“Russamento e sindrome delle apnee ostruttive nel sonno (OSAS): dal sintomo alla malattia” - 2020 
*	“Gestione del paziente tracheostomizzato ventilato” - 2020 
*	“Trattamento non chirurgico della malattia parodontale con training pratico su manichini” - 2020 
*	“fotografia odontoiatrica” – 2020
*	“Gnatologia: occlusione e funzioni dell’apparato masticatorio. Fondamenti di gnatologia per la riabilitazione restaurativo-protesica. Diagnosi e trattamento” - 2020 
*	“Perfezionamento universitario in scienza dell’alimentazione: nutrigenetica, nutrigenomica, nutraceutica ed apigenetica.” - 2020 
*	“Ipnosi e comunicazione ipnotica in pediatria” - 2020 
*	“Postural orthodontic therapy” - 2020 
*	“Gestione del paziente oncologico” - 2020 
*	“Corso teorico pratico di terapia parodontale non chirurgica” - 2020 