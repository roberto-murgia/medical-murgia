+++ 
tipo = "Curriculum"
title = "Ambra Sedran"
id = "Odrtodonzista"
date = ""
mail = "Ambra.Sedran@medicalmurgia.it"
tags = [""]
categories = [""]
banner = "img/banners/banner-4.jpg"
author = "Ambra Sedran"
immagine = "img/Staff_Ext_MM/Medico2.png"
titolo1 = "Dott.ssa  Ambra Sedran"
subtitolo = "Odontoiatra"
telefono = "Tel. 011 331794"
+++



<b>Formazione</b>


Diploma di Maturità Scientifica presso Liceo Scientifico E. Majorana nel 2012. 
Laurea Magistrale a Ciclo Unico in Odontoiatria e Protesi Dentaria conseguita a pieni voti presso l’Università di Torino a luglio 2018. 
Abilitazione professionale in Odontoiatria e Protesi Dentaria a novembre 2018. 
Scuola di formazione in Ortognatodonzia dal 2018 ad oggi. 
Tirocini clinici professionalizzanti presso la Dental School di Torino nei reparti di Endodonzia, Conservativa, 
Chirurgia Orale, Parodontologia, Ortognatodozia, Gnatologia, Patologia Orale, Pedodonzia e Protesi Dentaria dal 2015 al 2018. 


<b>Collaborazioni professionali</b>

Consulenza presso lo Studio Dentistico Dott.ri Sedran 
Consulenza presso Smile Dental Clinic 
Consulenza presso Studio Medico Dentistico G.Da. S.n.c. di Sottil Carlo & C. 
Consulenza ortodontica presso DentalFeel (Santa Vittoria d’Alba) 

<b>AttivitÃ  attuali</b>

Consulenza ortodontica e odontoiatrica conservativa presso Medical Murgia S.r.l. da dicembre 2019 ad oggi. 

<b>Pubblicazioni</b>

Articoli:

* “Percezione estetica e accettabilità delle apparecchiature ortodontiche di ultima generazione” - Rossini G., Sedran A., Parrini S., Sanna F., Deregibus A. - “Il Dentista Moderno” anno XXXVI, numero 6, giugno 2018 

Poster:  

* “La Terapia laser LLLT nel trattamento dei disordini temporomandibolari (TMD)” – Sedran A., Ambrogio G., Sedran V., Sedran A., Carossa S. - II International Meeting S.I.L.O. / I.S.L.D., VIII congresso nazionale S.I.L.O. 12-13 June 2015 Roma 
* “Utilizzo della nanotecnologia taopatch per curre odontoiatriche in soggetti HCP” - Sedran A., Rizzi R., Sindici E., Sedran A. - 19° Congresso Nazionale S.I.O.H. 5-6-7 Ottobre 2017 Milano 
* “Appropriateness and limits of orthodontic treatment of anterior crowding in adults with Invisalgn: a case report” - Sedran A., Grifalconi E., Tigani C., Saettone M., Castroflorio T. - 49th SIDO International Congress 16-17 March, Naples  
* “Agenesis of the maxillary lateral incisor: a new proposal for the choice of treatment” - Sedran A., Romani M., Ubertalli L., Barra F., Piancino M. G. - 49th SIDO International Congress 16-17 March, Naples 
* “Correction of a dental deep bite with functional appliance: a case report” - Rolfo M., Stoppello L., Saettone M., Sedran A., Piancino MG.  - 49th SIDO International Congress 16-17 March, Naples 
* “Research of radiology predictability factors of maxillary canine inclusion” – Rolfo M., Sedran A., Grifalconi E., Romani M., Piancino MG - 49th SIDO International Congress 16-17 March, Naples 
* “Analisi della percezione estetica e del valore economico percepito delle apparecchiature ortodontiche da parte di persone comuni” - Sedran A., Rossini G., Cortona A., Piancino M.G., Deregibus A., Castroflorio T. - 25° Congresso Nazionale Collegio dei Docenti Universitari di discipline Odontostomatologiche, Roma 12-14 Aprile 2018 
* “Effetti scheletrici e dentali di due apparecchiature funzionalizzanti in pazienti con II classe scheletrica in età prepuberale e puberale: Uno studio retrospettivo” – Barra F., Sedran A., Spadaro F., Castroflorio T., Piancino MG., Deregibus A. - 25° Congresso Nazionale Collegio dei Docenti Universitari di discipline Odontostomatologiche, Roma 12-14 Aprile 2018 
* “Device odontoiatrici nel trattamento dell’apnea ostruttiva del sonno nell’adulto” – Sedran A., Borio G., Sedran A., Gassino G. - 25° Congresso Nazionale Collegio dei Docenti Universitari di discipline Odontostomatologiche, Roma 12-14 Aprile 2018 
* “Gestione dell’apertura orale e della tensione muscolare in pazienti con disturbi temporo-mandibolari (TMD tramite laser terapia di basso livello (LLLT)” – Sedran A., Sedran A., Rizzi R. - 25° Congresso Nazionale Collegio dei Docenti Universitari di discipline Odontostomatologiche, Roma 12-14 Aprile 2018 
* “Validation of a dedicated software program for applying the ABO objective grading system and discrepancy index to digital models” - Sedran A., Rossini G., Deregibus A., Castroflorio T. - 49th SIDO International Congress 11-13 October 2018, Florence 
* “Bolton analysis on study models: validation of a dedicated software” - Sedran A., Rossini G., Deregibus A., Castroflorio T. - 49th SIDO International Congress 11-13 October 2018, Florence 
* “Reliability and validity of PAR index measurements detected automatically by a dedicated software” - Sedran A., Rossini G., Deregibus A., Castroflorio T. - 49th SIDO International Congress 11-13 October 2018, Florence 






