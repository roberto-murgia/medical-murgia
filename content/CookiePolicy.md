+++
title = "Cookie Policy"
description = "Frequently asked questions"
keywords = [""]
+++



# Cookie Policy

Questo sito o il “Sito” utilizza i Cookie per rendere i propri servizi semplici e efficienti per l’utenza che visiona le pagine del Sito.
Gli utenti che visionano il Sito, vedranno inserite delle quantità minime di informazioni nei dispositivi in uso, che siano computer e periferiche mobili, in piccoli file di testo denominati “cookie” salvati nelle directory utilizzate dal browser web dell’Utente.
Vi sono vari tipi di cookie, alcuni per rendere più efficace l’uso del Sito, altri per abilitare determinate funzionalità.
Analizzandoli in maniera particolareggiata i nostri cookie permettono di:

* memorizzare le preferenze inserite
* evitare di reinserire le stesse informazioni più volte durante la visita quali ad esempio nome utente e password


## Tipologie di Cookie utilizzati dal Sito

A seguire i vari tipi di cookie utilizzati dal Sito in funzione delle finalità d’uso

## Cookie Tecnici

Questa tipologia di cookie permette il corretto funzionamento di alcune sezioni del Sito. Sono di due categorie: persistenti e di sessione:

* persistenti: una volta chiuso il browser non vengono distrutti ma rimangono fino ad una data di scadenza preimpostata
* di sessioni: vengono distrutti ogni volta che il browser viene chiuso

Questi cookie, inviati sempre dal nostro dominio, sono necessari a visualizzare correttamente il sito e in relazione ai servizi tecnici offerti, verranno quindi sempre utilizzati e inviati, a meno che l’utenza non modifichi le impostazioni nel proprio browser (tale azione potrebbe comportare una non corretta visualizzazione dei contenuti del Sito).

## Cookie analitici

I cookie in questa categoria vengono utilizzati per collezionare informazioni sull’uso del sito.
 
#### Google Analytics

Questo sito web utilizza Google Analytics, un servizio di analisi web fornito da Google, Inc. (“Google”). Google Analytics utilizza dei cookies, che sono file di testo depositati sul Vostro computer per consentire al sito web di analizzare come gli utenti utilizzano il sito. Le informazioni generate dal cookie sull’utilizzo del sito web (compreso il tuo indirizzo IP anonimo) verranno trasmesse e depositate presso i server di Google negli Stati Uniti. Google utilizzerà queste informazioni allo scopo di esaminare il tuo utilizzo del sito web, compilare report sulle attività del sito per gli operatori dello stesso e fornire altri servizi relativi alle attività del sito web e all’utilizzo di internet. Google può anche trasferire queste informazioni a terzi ove ciò sia imposto dalla legge o laddove tali terzi trattino le suddette informazioni per conto di Google. Google non assocerà il tuo indirizzo IP a nessun altro dato posseduto da Google.
Puoi rifiutarti di usare i cookies analitici modificando le impostazioni del browser, ma così facendo puoi non essere in grado di utilizzare tutte le funzionalità di questo sito.
Navigando le pagine del sito, acconsenti al trattamento dei tuoi dati da parte di Google per le modalità ed i fini sopraindicati.

#### Siti Web e servizi di terze parti

Il Sito potrebbe contenere collegamenti ad altri siti Web che dispongono di una propria informativa sulla privacy che può essere diverse da quella adottata dal Sito. Per l’informativa sull’uso di tali cookies si rimanda agli specifici siti delle terze parti.


## Cookie utilizzati dal Sito

A seguire la lista di cookie tecnici e di analytics utilizzati da questo Sito:

* Cookie tecnici: cookie di sessione,
* Cookie analitici: Google Analytics,
* Cookie di terze parti: nessuno.

## Gestione dei Cookie mediante configurazione del browser

L’utente può configurare il proprio browser per accettare o rifiutare automaticamente tutti i cookie o per ricevere sullo schermo un avviso della trasmissione di ciascun cookie e decidere di volta in volta se installarlo o meno sull’hard disk. Per un dettaglio sulle modalità di esecuzione di tale operazione si suggerisce di consultare la sezione “Aiuto” del browser utilizzato. Fatto salvo quanto precede, è necessario tener presente che la disattivazione dei cookies potrebbe condizionare il corretto funzionamento di determinate sezioni del Sito. Di seguito si forniscono indicazioni per la configurazione di tali impostazioni per i browser più diffusi:

## Chrome

* Eseguire il Browser Chrome
* Fare click sul menù presente nella barra degli strumenti del browser a fianco della finestra di inserimento url per la navigazione
* Selezionare Impostazioni
* Fare clic su Mostra Impostazioni Avanzate
* Nella sezione “Privacy” fare clic su bottone “Impostazioni contenuti”
* Nella sezione “Cookie” è possibile modificare le seguenti impostazioni relative ai cookie:
  *  Consentire il salvataggio dei dati in locale
  *  Modificare i dati locali solo fino alla chiusura del browser
  *  Impedire ai siti di impostare i cookie
  *  Bloccare i cookie di terze parti e i dati dei siti
  *  Gestire le eccezioni per alcuni siti internet
  *  Eliminazione di uno o tutti i cookie
Per maggiori informazioni si rimanda al sito web del distributore del software.

## Mozilla Firefox

* Eseguire il Browser Mozilla Firefox
* Fare click sul menù presente nella barra degli strumenti del browser a fianco della finestra di inserimento url per la navigazione
* Selezionare Opzioni
* Seleziona il pannello Privacy
* Fare clic su Mostra Impostazioni Avanzate
* Nella sezione “Privacy” fare clic su bottone “Impostazioni contenuti”
Nella sezione “Tracciamento” è possibile modificare le seguenti impostazioni relative ai cookie:
  * Richiedi ai siti di non effettuare alcun tracciamento
  * Comunica ai siti la disponibilità ad essere tracciato
  * Non comunicare alcuna preferenza relativa al tracciamento dei dati personali
* Dalla sezione “Cronologia” è possibile:
  * Abilitando “Utilizza impostazioni personalizzate” selezionare di accettare i cookie di terze parti (sempre, dai siti più visitato o mai) e di conservarli per un periodo determinato (fino alla loro scadenza, alla chiusura di Firefox o di chiedere ogni volta)
  * Rimuovere i singoli cookie immagazzinati
  
Per maggiori informazioni si rimanda al sito web del distributore del software.

## Internet Explorer

* Eseguire il Browser Internet Explorer
* Fare click sul pulsante Strumenti e scegliere Opzioni Internet
  * Fare click sulla scheda Privacy e nella sezione Impostazioni modificare il dispositivo di scorrimento in funzione dell’azione desiderata per i cookie: Bloccare tutti i cookie
  * Consentire tutti i cookie
  * Selezione dei siti da cui ottenere cookie: spostare il cursore in una posizione intermedia in modo da non bloccare o consentire tutti i cookie, premere quindi su Siti, nella casella Indirizzo Sito Web inserire un sito internet e quindi premere su Blocca o Consenti

Per maggiori informazioni si rimanda al siti web dei distributori del software.

## Safari

* Eseguire il Browser Safari
* Fare click su Safari, selezionare Preferenze e premere su Privacy
* Nella sezione Blocca Cookie specificare come Safari deve accettare i cookie dai siti internet.
* Per visionare quali siti hanno immagazzinato i cookie cliccare su Dettagli

Per maggiori informazioni si rimanda al siti web dei distributori del software.

> Questa pagina è visibile, mediante link in calce in tutte le pagine del Sito ai sensi dell’art. 122 secondo comma del D.lgs. 196/2003 e a seguito delle modalità semplificate per l’informativa e l’acquisizione del consenso per l’uso dei cookie pubblicata sulla Gazzetta Ufficiale n.126 del 3 giugno 2014 e relativo registro dei provvedimenti n.229 dell’8 maggio 2014.

