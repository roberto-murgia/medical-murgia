+++
title = "Servizi alla persona"
id = "Dietista"
mail = "Maria.Santa@medicalmurgia.it"
date = ""
tags = [""]
categories = [""]
banner = "img/banners/banner-4.jpg"
author = "Maria Santa"
immagine = "img/Servizi/Odontoiatria.png"
blog ="N"

+++

# Consulenze Nutrizionali

"Se fossimo in grado di fornire a ciascuno la giusta dose di nutrimento ed esercizio fisico, né in difetto né in eccesso, avremmo trovato la strada per la salute"
​Ippocrate (460-377 a.C.)
​


<figure style="margin-bottom:0px; margin-right:0px; margin-left:0px;  width:100%"/>
  <img src="../../img/Servizi/Nutrizione1.png" style="margin-bottom:20px; width:100%"/>
</figure> 



L’organismo umano, per funzionare correttamente, necessita di proteine, grassi, carboidrati, vitamine e sali minerali. L’apporto energetico giornaliero deve essere 
adeguato al consumo calorico, ma non è solo importante la quantità!
L'attenzione deve essere rivolta anche (e soprattutto) alla qualità e alla varietà degli alimenti che assumiamo, ai nostri gusti personali, alle nostre esigenze
 (tempo, lavoro, famiglia, pasti fuori casa) e abitudini: un'alimentazione cucita appositamente per sé!
La Dieta non deve essere una tortura, una rinuncia ai piaceri della tavola e una lotta contro il girovita; l'obiettivo deve essere una Dieta intesa nel senso 
più ampio del termine (cioè “stile di vita”): una gestione consapevole del cibo nel rispetto del proprio corpo!

<figure style="margin-bottom:0px; margin-top:25px;margin-right:0px; margin-left:10px; float:right; width:50%"/>
  <img src="../../img/Servizi/Nutrizione2.png" style="margin-bottom:20px; width:100%"/>
</figure>  
​
<li>vuoi migliorare la propria forma fisica e il proprio benessere generale?</li>
<li>vuoi perdere massa grassa in maniera sana, equilibrata e duratura            </li> 
<li>segui un regime vegetariano o vegano? </li> 
<li>vuoi aumentare la massa muscolare?</li> 
<li>vuoi associare ad una pratica sportiva agonistica una corretta e bilanciata alimentazione?</li> 
<li>bambini in sovrappeso che devono modificare il loro stile di vita?</li> 
<li>sei una donna in gravidanza e in allattamento, devi far fronte a nuove necessità?</li> 
<li>bimbi, dallo svezzamento in poi (6m+)</li> 
<li>sei una donna in menopausa, devi affrontare nuovi equilibri?</li> 

                                                                            
<br><br>
	


<a href="/medical-murgia/06-contatti/"> <h3>Contattaci !</h3> </a>



<figure style="margin-bottom:0px; margin-right:0px; margin-left:0px;  width:100%"/>
  <img src="../../img/Servizi/Nutrizione3.png" style="margin-bottom:20px; width:100%"/>
</figure> 
