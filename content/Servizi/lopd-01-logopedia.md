+++
title = "Servizi alla persona"
id = "Logopedia"
date = ""
mail = "xxxxxxxxxxx.xxxxxxxxxxxx@medicalmurgia.it"
tags = [""]
categories = [""]
banner = "img/banners/banner-4.jpg"
author = "XXXXXXXXX YYYYYYY"
immagine = "img/Servizi/Odontoiatria.png"
blog ="N"

+++

# Logopedia

La <b>logopedia</b> si occupa dello studio, della prevenzione, della valutazione e della cura delle patologie e dei disturbi della voce, 
del linguaggio, della comunicazione, della deglutizione e dei disturbi cognitivi connessi (relativi, ad esempio, alla memoria e all'apprendimento). 

<br>
	
<figure style="margin-bottom:0px; margin-right:0px; margin-left:0px;  width:100%"/>
  <img src="../../img/Servizi/Logopedia1.png" style="margin-bottom:20px; width:100%"/>
</figure> 

	
<h3>VISITA LOGOPEDICA</h3>
	
Casella di testoDurante la <b>prima visita</b> il logopedista individua le <b>specifiche difficoltà del paziente</b> così da poter impostare 
un trattamento mirato alle sue problematiche. Viene, quindi, effettuata <b>un’indagine anamnestica</b>, ossia lo specialista raccoglie 
tutte le informazioni utili a definire con maggior precisione il deficit insorto. 

Viene, inoltre, chiesto al paziente di eseguire <b>test standardizzati</b>, i quali fanno emergere le caratteristiche del disturbo, 
come ad esempio il <b>PPVT</b> (test utilizzato con i bambini con sospetto disturbo del linguaggio), per verificare le loro capacità di comprensione lessicale. 



<h3>SEDUTE DI LOGOPEDIA </h3>


La seduta di logopedia inizia con un <b>colloquio</b> con il paziente e con i suoi cari, momento in cui il logopedista fornisce informazioni e consigli circa i 
<b>comportamenti corretti</b> da adottare per risolvere la problematica insorta. 

Al paziente viene consigliato di eseguire attività selezionate ad hoc come <b>esercizi</b> di logopedia, al fine di ridurre e, progressivamente, 
eliminare le difficoltà emerse al momento della valutazione. 

Si eseguono sedute per: 

<figure style="margin-bottom:0px; margin-right:50px; margin-left:10px; float:right; width:50%"/>
  <img src="../../img/Servizi/logopedia2.png" style="margin-bottom:20px; width:100%"/>
</figure>  

* Deglutizione atipica infantile 
* Monitoraggio e accompagnamento dei trattamenti ortodontici 
* Disfagia (alterazione della deglutizione) 
* Disfonia (disturbi della voce) 
* Disturbo del linguaggio 
* Disturbi dell’apprendimento (dislessia, discalculia, disortografia) 
* Afasia (disturbo della comunicazione) 
* Disartria (deficit di articolazione verbale) 
* Sordità 
* Balbuzie 
* Disturbi legati a malattie degenerative (Alzheimer, Perkinson, SLA) 

