+++
title = "Medicina Specialistica"
id = "Pediatria"
date = ""
mail = "Antonella.Cerutti@medicalmurgia.it"
tags = [""]
categories = [""]
banner = "img/banners/banner-4.jpg"
author = "Dott.ssa ANTONELLA CERUTTI"
immagine = "img/Servizi/Odontoiatria.png"
blog ="N"

+++

# Pediatria

<b>La pediatria è una branca della medicina che si occupa della salute e dell'assistenza medica degli infanti, 
dei bambini e degli adolescenti dalla nascita fino all'età  di 18 anni. </b><br><br>

Il medico pediatra si occupa sia dello sviluppo psicofisico che della diagnosi e terapia delle malattie infantili.


<h3>VISITA PEDIATRICA</h3>

  <figure style="margin-bottom:0px; margin-right:20px; margin-left:50px;  width:100%"/>
  <img src="../../img/Servizi/Pediatria1.png" style="margin-bottom:20px; width:100%"/>
</figure> 


La visita pediatrica è sempre un momento molto delicato. E’ importante che si svolga tranquillamente e senza fretta in modo tale da permettere al bambino di adattarsi al nuovo ambiente sia per garantire al genitore lo spazio di dialogo adeguato con il pediatra. 

La visita viene effettuata da un medico specialista in pediatria al fine di controllare l’andamento dello sviluppo del bambino o di valutare le eventuali patologie tipiche dell’età evolutiva. 

I bambini e le loro famiglie sono al centro del nostro impegno: promuovere ed educare alla salute in un ambiente accogliente e sereno. 

Contattaci per maggiori informazioni o per prenotare una visita. (???) 


<figure style="margin-bottom:0px; margin-right:50px; margin-left:0px; float:LEFT; width:50%"/>
  <img src="../../img/Servizi/Pediatria2.png" style="margin-bottom:20px; width:100%"/>
</figure>  