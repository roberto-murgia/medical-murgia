+++
title = "Medicina Specialistica"
id = "Medicina del Lavoro"
date = ""
mail = "Alessandro.turrini@medicalmurgia.it"
tags = [""]
categories = [""]
banner = "img/banners/banner-4.jpg"
author = "Alessandro Turrini"
immagine = "img/Servizi/Odontoiatria.png"
blog ="N"

+++

# Medicina del Lavoro

Presso Medical Murgia è attivo il servizio di Medicina del Lavoro in grado di fornire alle <b>aziende </b>
una qualificata consulenza in merito a tutti gli adeguamenti richiesti dalle attuali normative. 
<br><br><br><br>

<figure style="margin-bottom:0px; margin-right:0px; margin-left:0px;  width:100%"/>
  <img src="../../img/Servizi/MedLav1.png" style="margin-bottom:20px; width:100%"/>
</figure> 

Si eseguono: 

* <b>Sopralluogo</b> sanitario per la definizione dei rischi e delle misure di prevenzione e
 protezione con <b>controlli</b> periodici degli <b>ambienti di lavoro</b>; 

* Sorveglianza sanitaria dei <b>lavoratori dipendenti</b> comprensiva di: 
	* Accertamento preventivo e valutazione della idoneità alla mansione; 
	* Accertamenti periodici dello stato di salute comprensivi di <b>visite del medico del lavoro</b> 
	(Il quale prescriverà, se necessario, visite specialistiche, esami e test di laboratorio, esami strumentali)

<figure style="margin-bottom:20px; margin-right:0px; margin-left:50px; float:right; width:50%"/>
  <img src="../../img/Servizi/MedLav2.png" style="margin-bottom:20px; width:100%"/>
</figure>

Le visite con il medico del lavoro possono essere effettuate, per un numero limitato di dipendenti e 
previo appuntamento, presso la nostra struttura. 

Contattaci tramite il form da compilare sul nostro sito per ricevere maggiori informazioni o 
richiedere un preventivo del servizio.  



<b>Medical Murgia srl</b> ha realizzato in collaborazione con <b>Medilav. Srl – Euronorma S.a.s.</b> 
il Software 81ML. <br>
<b>81ML</b> è l’evoluzione di Sintalex-Medilav, rilasciato nel 2008, in coincidenza con l’approvazione 
del D.Lgs. 81/08.

<b>81ML</b> software è sviluppato dai Medici per i Medici
Mentre gli altri Software in commercio sono ideati dagli informatici con la consulenza dei medici per 
rispondere prevalentemente ad esigenze esterne ed estranee alla attività medica, <b>81ML</b>, come il predecessore, 
è concepito e realizzato da Medici Competenti e Autorizzati, con la consulenza degli informatici, 
per rispondere alle esigenze di chi lo deve usare: i Medici Competenti e Autorizzati per l’effettuazione 
della sorveglianza sanitaria (D.Lgs.81/08) e medica (D.Lgs.230/95).

  <figure style="margin-bottom:0px; margin-right:20px; margin-left:0px; float:left; width:50%"/>
  <img src="../../img/Servizi/MedLav3.png" style="margin-bottom:20px; width:100%"/>
</figure> 
Principali funzionalità di 81ML:
Importare le anagrafiche di aziende, unità produttive, lavoratori, datori di lavoro, RSPP, RLS, da file EXCEL
Associare i rischi alle mansioni e definire i protocolli, anche in automatico mediante protocolli standard 
proposti dal software, che possono essere accettati ovvero modificati;
Inserire i dati delle visite nella Cartella Sanitaria e di Rischio/DSP con supporti per costruire 
frasi-tipo senza digitazione su tastiera;
Formulare le prescrizioni, le limitazioni e le condizioni per l’idoneità con supporti guidati per 
costruire frasi ad hoc senza digitazione su tastiera;
Effettuare la sorveglianza sanitaria e medica anche mediante l’utilizzo di checklist e questionari, 
come il questionario anamnestico per DMS epm, Audit C,  integrati nella cartella sanitaria;
Produrre e stampare (in PDF o cartaceo) le copie dei giudizi di idoneità da consegnare a lavoratori e 
datori di lavoro, la cartella sanitaria e di rischio/DSP e altri report;
Gestire in automatico gli scadenzari di visite, esami, vaccinazioni, ecc.
Elaborare in automatico le “relazioni sanitarie”  con l’analisi statistica dei dati anagrafici, 
clinici e degli accertamenti sanitari e produzione di  report editabile in Word;
Elaborare automaticamente i dati per l’allegato 3B su file di EXCEL pronto per essere caricato sul portale INAIL
 