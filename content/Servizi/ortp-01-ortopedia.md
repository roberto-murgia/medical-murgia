+++
title = "Medicina Specialistica"
id = "Ortopedia"
mail = "Matteo.Ferretti@medicalmurgia.it"
date = ""
tags = [""]
categories = [""]
banner = "img/banners/banner-4.jpg"
author = "Matteo Ferretti"
immagine = "img/Servizi/Odontoiatria.png"
blog ="N"

+++

# Ortopedia

L’ortopedia è una branca iperspecialistica della chirurgia e si occupa dello studio delle 
patologie che colpiscono l’apparato locomotore.

Il medico ortopedico si occupa principalmente delle patologie legate alla traumatologia, cioè conseguenti ad un evento traumatico che ha compromesso la funzionalità motoria del paziente. 
Presso Medical Murgia possiamo contare su medici esperti in grado di valutare con cura le problematiche del paziente e orientarlo verso un percorso di cura personalizzato. 
 

<figure style="margin-bottom:20px;margin-top:20px;margin-right:0px; margin-left:0px;  max-width:100%"/>
  <img src="../../img/Servizi/ortopedia1.png" style="margin-bottom:20px; width:100%"/>
</figure>



<h3> VISITA SPECIALISTICA ORTOPEDICA  </h3>

La visita viene effettuata da un medico specializzato in Ortopedia e Traumatologia (?) 
al fine di valutare le patologie che colpiscono l’apparato muscolo-scheletrico. 

<figure style="margin-bottom:0px; margin-right:50px; margin-left:10px; float:LEFT; width:50%"/>
  <img src="../../img/Servizi/ortopedia2.png" style="margin-bottom:20px; width:100%"/>
</figure>  
Durante la visita, l’ortopedico procede all’esame anamnestico relativo allo stato di 
salute generale, ai sintomi descritti, alla storia familiare e clinica specifica del paziente. 
Successivamente all’anamnesi, il medico provvede all’esame obiettivo, eseguendo test clinici 
specialistici mirati alla valutazione della problematica. La prescrizione o la valutazione di 
esami strumentali già in possesso del paziente (Radiografie, ecografie TAC, RMN, ….) 
completa la visita specialistica ortopedica. <br> <br><br> <br>
Al termine della visita specialistica l’ortopedico instraderà il paziente in un percorso terapeutico personalizzato. 

Contattaci per maggiori informazioni o per prenotare una visita. 