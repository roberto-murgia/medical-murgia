+++
title = "Odontoiatria"
id = "Protesi"
date = ""
mail = "marco.murgia@medicalmurgia.it"
tags = [""]
categories = [""]
banner = "img/banners/banner-4.jpg"
author = "Marco Murgia"
immagine = "img/Servizi/Odontoiatria.png"
blog ="N"
link = "servizi/odon-01-odonto"
livello = "../../../"
+++

# Protesi

Denti singoli, ponti, protesi parziali o totali: troveremo la soluzione migliore che si adatti alle singole esigenze dei nostri pazienti.<br> 
La <b>protesi</b> è un manufatto che consente di sostituire uno o più elementi dentali mancanti e si differenzia in due grandi categorie: 

<figure style="margin-bottom:20px;margin-top:20px;margin-right:0px; margin-left:0px;  max-width:100%"/>
  <img src="../../../img/Servizi/protesi1.png" style="margin-bottom:20px; width:100%"/>
</figure>

<h3>Protesi Fissa </h3>

quando appoggia su elementi dentari naturali e/o artificiali (impianti) cementati o avvitati in maniera permanente. 

 

 Si eseguono: 

* Corone e ponti in ceramica su denti naturali e impianti 
* Protesi fissa su 4 o 6 impianti 

 

Sceglieremo insieme il materiale più adeguato e con l’aiuto dello spettrofotometro potremo ricreare perfettamente il colore naturale dei tuoi denti. 


  <figure style="margin-bottom:0px; margin-right:20px; margin-left:50px; float:right; width:50%"/>
  <img src="../../../img/Servizi/Protesi2.png" style="margin-bottom:20px; width:100%"/>
</figure> 

<h3>Protesi Rimovibile </h3>
quando il manufatto protesico è progettato per essere rimosso dalla cavità orale dal paziente stesso per essere pulito; può sostituire alcuni elementi dentari (protesi parziale), o tutta una arcata (protesi totale). 

 

  Si eseguono: 

* Scheletrato con ganci 
* Scheletrato con attacchi di precisione 
* Protesi totale 
* Overdenture su impianti 

 