+++
title = "odontoiatria"
id = "Ortodonzia-Invisalign"
date = ""
mail = "marco.murgia@medicalmurgia.it"
tags = [""]
categories = [""]
banner = "img/banners/banner-4.jpg"
author = "Marco Murgia"
immagine = "img/Servizi/Odontoiatria.png"
blog ="N"
link = "servizi/odon-01-odonto"
livello = "../../../"
+++

# Ortodonzia-Invisalign®


L’ortodonzia è la branca dell’odontoiatria che riguarda la prevenzione e il trattamento delle 
anomalie di posizione e di sviluppo di denti e ossa mascellari. Questo tipo di trattamento può 
essere eseguito sia nei bambini che negli adulti attraverso l’applicazione di apparecchi mobili o fissi. 
<br><br><br>

<h3> ORTODONZIA MOBILE  </h3>

  <figure style="margin-bottom:0px; margin-right:20px; margin-left:50px; float:right; width:50%"/>
  <img src="../../../img/Servizi/InvisaL2.png" style="margin-bottom:20px; width:100%"/>
</figure> 
Sfrutta apparecchiature funzionali, <b>rimovibili</b> e riposizionabili a seconda della necessità nella bocca. 
Questi <b>apparecchi</b> vengono in genere utilizzati per intervenire su pazienti in fase di sviluppo. 
Per questo motivo sono primariamente rivolti a <b>pazienti in età evolutiva</b> (3 – 13 anni). 
<br>
<br>

La terapia può essere eseguita tramite il classico apparecchio in resina e filo metallico, 
oppure tramite l’innovativo sistema <b>Occlus-o-Guide®</b>, un dispositivo realizzato in materiale 
plastico morbido che garantisce <b>funzionalità</b> ed massimo <b>comfort</b> per il paziente. (Link sito Sweden? 
à Loro possono linkarci come centro Occlus-o-guide?) 

<br>

<h3> ORTODONZIA FISSA   </h3>

  <figure style="margin-bottom:0px; margin-right:20px; margin-left:0px; float:left; width:50%"/>
  <img src="../../../img/Servizi/InvisaL3.png" style="margin-bottom:20px; width:100%"/>
</figure> 
È’ il sistema <b>tradizionale</b> per l’allineamento dei denti. Consiste nella collocazione di <b>attacchi</b> (brackets) 
di <b>metallo</b> o <b>ceramica</b> sopra i denti. È’ rivolta ai pazienti, bambini e adulti, in dentizione definitiva. 

<br><br><br><br><br><br><br><br><br><br><br>

<h3> ORTODONZIA INVISIBILE – SISTEMA INVISALIGN®   </h3>


<figure style="margin-bottom:20px; margin-right:0px; margin-left:0px;  max-width:100%"/>
  <img src="../../../img/Servizi/InvisaL1.png" style="margin-bottom:20px; width:100%"/>
</figure>
 

Il sistema <b>Invisalign®</b> consente di allineare gradualmente i denti attraverso delle mascherine 
rimovibili personalizzate e quasi del tutto <b>invisibili</b>. Viene sfruttata un’<b>avanzata tecnologia</b> 
computerizzata che attraverso immagini in 3D simula l’intero percorso ortodontico del paziente. 
Tutto ciò consente al paziente di migliorare il proprio sorriso senza interferire con le sue abitudini. 
(Link sito Invisalign? à Loro possono linkarci come “distributori”?) 
