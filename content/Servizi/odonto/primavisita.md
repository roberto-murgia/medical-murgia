+++
title = "odontoiatria"
id = "PrimaVisita"
date = ""
mail = "marco.murgia@medicalmurgia.it"
tags = [""]
categories = [""]
banner = "img/banners/banner-4.jpg"
author = "Marco Murgia"
immagine = "img/Servizi/Odontoiatria.png"
blog ="N"
link = "servizi/odon-01-odonto"
livello = "../../../"
+++
 
# Prima Visita

La prevenzione è la base per mantenere in salute la tua bocca, per questo presso Medical Murgia 
la prima visita odontoiatrica è <b>gratuita</b>.  
E' però necessaria la <b>prenotazione</b>, in questo modo potremo dedicarti il nostro tempo e valutare 
il tuo caso con tranquillità. <br><br>
Durante la prima visita odontoiatrica troverai un'atmosfera accogliente e la nostra attenzione sarà focalizzata sulle tue esigenze.  <br>
E' una visita odontoiatrica specialistica, completa di<b> panoramica</b> e/o radiografie endorali,
 test diagnostici, diagnosi del dentista, <b> piano di cure</b> odontoiatriche e <b>preventivo</b> con definizione delle 
 modalità  di pagamento. <br><br>



<figure style="margin-bottom:20px; margin-right:0px; margin-left:0px;  max-width:100%"/>
  <img src="../../../img/Servizi/PrimaVisita.png" style="margin-bottom:20px; width:100%"/>
</figure>


La prima visita dentistica prevede diverse fasi: 
 * Compilazione dell'<b>anamnesi</b> medico odontoiatrica 
 * Visita dentistica con <b>radiografie</b> e/o <b>panoramica  </b>
 * Studio e consegna del piano di terapia 
 * Consegna del <b>preventivo</b> e definizione modalità  di <b>pagamento </b>

Procedere in questo modo ci permette di conoscerci meglio ed essere informati sulla tua storia clinica, 
così da poter formulare un piano di trattamento specifico e <b>personalizzato</b>, creato in base alle necessità 
mediche ma senza trascurare le richieste personali. 






<h3>CONTROLLI PERIODICI </h3>
<figure style="margin-bottom:20px; margin-right:0px; margin-left:50px; float:right; width:50%"/>
  <img src="../../../img/Servizi/PrimaVisita2.png" style="margin-bottom:20px; width:100%"/>
</figure>

<b>A tutti i nostri pazienti offriamo inoltre un programma di controlli periodici</b>.  
<br>
Programmare i controlli è fondamentale per intervenire per tempo su un problema che nel tempo 
potrebbe trasformarsi in un danno maggiore o irreparabile.  
<br>
A questo scopo aderiamo al "Mese della prevenzione dentale" nato dalla collaborazione tra ANDI 
(Associazione Nazionale Dentisti Italiani) e Mentadent.  
<br>
Tieni d'occhio la nostra sezione News per prenotare la tua visita di controllo durante il mese 
della prevenzione e ricevere un piccolo omaggio.
<br> 
>E ricorda... <b>PREVENIRE E' MEGLIO CHE CURARE</b>! 