+++
title = "Odontoiatria"
id = "Odontoiatria infantile"
date = ""
mail = "marco.murgia@medicalmurgia.it"
tags = [""]
categories = [""]
banner = "img/banners/banner-4.jpg"
author = "Marco Murgia"
immagine = "img/Servizi/Odontoiatria.png"
blog ="N"
link = "servizi/odon-01-odonto"
livello = "../../../"
+++

# Odontoiatria infantile


La <b>pedodonzia</b> è la branca dell'odontoiatria che si prende cura della salute orale dei bambini. 
Riguarda soprattutto la cura delle 
<b>carie</b> e la <b>prevenzione</b>. 

Poniamo grande attenzione alla prevenzione tramite l'educazione alle corrette abitudini di igiene orale e 
la collaborazione della 
famiglia e dei piccoli pazienti. 

 

Dopo la prima visita dentistica il percorso di cura e prevenzione prosegue con un approccio <b>multidisciplinare</b>. 
I nostri piccoli pazienti potranno infatti avvalersi della collaborazione sinergica di <b>professionisti</b> 
nel campo dell'igiene dentale, 
dell'ortodonzia, della logopedia e della pediatria. 

<figure style="margin-bottom:20px;margin-top:20px;margin-right:0px; margin-left:0px;  max-width:100%"/>
  <img src="../../../img/Servizi/bimbi1.png" style="margin-bottom:20px; width:100%"/>
</figure>

Si eseguono: 

* Visite di prevenzione (A partire dai 3 anni) 
* Istruzioni di igiene 
* Detartrasi 
* Fluoroprofilassi 
* Terapia ortodontica 
* Sigillatura denti (da latte e permanenti) 
* Cura dei denti da latte 


<h3> PRIMA VOLTA DAL DENTISTA?  </h3>
 

  <figure style="margin-bottom:0px; margin-right:20px; margin-left:50px; float:right; width:50%"/>
  <img src="../../../img/Servizi/bimbi2.png" style="margin-bottom:20px; width:100%"/>
</figure> 

Ecco alcuni consigli per preparare i bambini all’appuntamento senza creare ansie o traumi bensì creando 
le basi perché sia un’esperienza positiva: 
* Fissare l’appuntamento in un giorno tranquillo, in cui non ci siano altri impegni o altre visite; 
* Preparare il bambino alla visita con un linguaggio positivo (Evitare di dire “non sentirai male”, 
“il dentista non ti fa niente”, “non devi avere paura” ecc.); 
* Utilizzare un vocabolario giocoso per descrivere ciò che avverrà dal dentista (“ti siederai su una 
poltrona spaziale”, “il dentista guarderà come crescono bene i tuoi dentini con un piccolo specchietto 
ed una lucina”, ecc.); 
* Prendere appuntamento in modo preventivo, non aspettare che il bambino abbia dolore in modo che 
possa essere un’esperienza piacevole non legata ad una sensazione di malessere; 
* Essere presenti nello studio durante la prima visita conoscitiva, mentre nelle sedute successive 
è utile che il genitore aspetti fuori in modo tale da poter creare un legame di fiducia tra 
il piccolo paziente ed il dentista. E’ consigliabile che il bambino venga accompagnato da un 
solo genitore perché la presenza di entrambi potrebbe far pensare ad una situazione grave. 