+++
title = "Odontoiatria"
id = "IgieneParodontologia"
date = ""
mail = "marco.murgia@medicalmurgia.it"
tags = [""]
categories = [""]
banner = "img/banners/banner-4.jpg"
author = "Marco Murgia"
immagine = "img/Servizi/Odontoiatria.png"
blog ="N"
link = "servizi/odon-01-odonto"
livello = "../../../"
+++

# Igiene orale e Parodontologia

<h3>IGIENE ORALE </h3>
L’igiene orale è la branca dell’odontoiatria che si occupa del mantenimento di una <b>corretta pulizia
 della bocca</b> ed è una prestazione <b>fondamentale</b> per la prevenzione. 
L’igiene orale, o <b>detartras</b>i, presso Medical Murgia viene eseguita dall<b>’Igienista Dentale</b>,
 una figura professionale laureata, specializzata in questo settore. Durante la seduta l’igienista valuterà
  il generale stato di <b>salute delle gengive</b> e l’eventuale presenza di<b> carie</b>. Al termine della seduta invece
   si soffermerà sulle corrette <b>tecniche di spazzolamento</b> per mantenere una corretta pulizia anche a casa. 
   <br>
      <br>

<figure style="margin-bottom:20px; margin-right:0px; margin-left:0px;  max-width:100%"/>
  <img src="../../../img/Servizi/Igiene1.png" style="margin-bottom:20px; width:100%"/>
</figure>
 
<h3> PERCHE’ È IMPORTANTE LA PULIZIA PROFESSIONALE DEL CAVO ORALE?  </h3>


Ogni volta che ingeriamo del cibo modifichiamo e alteriamo il PH interno della bocca e 
attraverso la masticazione portiamo a contatto cibo e denti lasciando dei <b> residui</b>. 

  <figure style="margin-bottom:0px; margin-right:20px; margin-left:0px; float:left; width:50%"/>
  <img src="../../../img/Servizi/Igiene2.png" style="margin-bottom:20px; width:100%"/>
</figure> 
Lo spazzolino spesso non riesce ad eliminare tutti i residui che si depositano nella cavità orale diventando, 
nel tempo,<b> placca</b> e <b>tartaro</b>. <br>Questi depositi sono ricchi di batteri e sono i responsabili di carie
, infiammazioni gengivali, tumori orali. <br><b> Alito cattivo</b> e <b>gengive infiammate</b> sono i primi sintomi
 a cui prestare attenzione. 
 
   <br><br> <br><br> <br><br><br>
 
 Si eseguono: 
 * Detartrasi 
 * Sigilli 
 * Lezioni di igiene 
 * Sbiancamenti (Laser o…?) 
 * Trattamenti desensibilizzanti (Laser) 
 * Fluoroprofilassi 
 

<h3> PARODONTOLOGIA  </h3>

<figure style="margin-bottom:20px; margin-right:0px; margin-left:50px; float:right; width:50%"/>
  <img src="../../../img/Servizi/Igiene3.png" style="margin-bottom:20px; width:100%"/>
</figure>

La <b>parodontologia</b> è il campo dell’odontoiatria che si occupa delle malattie parodontali 
come la <b>parodontite</b> (piorrea) e la <b>gengivite</b>. Queste patologie sono molto diffuse nella popolazione 
e sono la causa principale della <b>perdita di denti</b> negli adulti. La migliore <b>prevenzione</b> è 
la combinazione di detartrasi e controlli periodici. <br>

Il <b>laser</b> è lo strumento maggiormente utilizzato in ambito terapeutico e preventivo della malattia parodontale. 
La combinazione della terapia non chirurgica (levigatura) con il laser determina infatti, nelle tasche 
parodontali profonde, la decontaminazione dell’area grazie al suo potere <b>battericida</b> e favorisce 
la guarigione dei tessuti con la sua azione <b>biostimolante</b>. 
<br>

 Si eseguono: 
 * Esami sistemici radiografici endorali  
 * Decontaminazione laser 
 * Cartelle parodontali 
 * Innesti connettivali 
 * Levigature (manuali e con laser) 
 * Chirurgia plastica gengivale 