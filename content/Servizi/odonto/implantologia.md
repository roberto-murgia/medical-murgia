+++
title = "Odontoiatria"
id = "ImplantologiaChirurgia"
date = ""
mail = "marco.murgia@medicalmurgia.it"
tags = [""]
categories = [""]
banner = "img/banners/banner-4.jpg"
author = "Marco Murgia"
immagine = "img/Servizi/Odontoiatria.png"
blog ="N"
link = "servizi/odon-01-odonto"
livello = "../../../"
+++

# Implantologia e Chirurgia orale

 
<h3> IMPLANTOLOGIA </h3>
L’ <b>implantologia</b> è una tecnica chirurgico-protesica mirata alla sostituzione di 
uno o più elementi dentari o alla riabilitazione totale di un’arcata dentale tramite 
l’utilizzo di impianti in <b>titanio</b>. Gli impianti si integrano nell’osso tramite 
un processo detto <b>osteointegrazione</b>. L’impianto sostituisce la radice naturale 
del dente con una radice “artificiale” sulla quale viene cementato o avvitato l’elemento protesico. 

<figure style="margin-bottom:20px;margin-top:20px;margin-right:0px; margin-left:0px;  max-width:100%"/>
  <img src="../../../img/Servizi/Implantologia1.png" style="margin-bottom:20px; width:100%"/>
</figure>
 
La chirurgia implantare viene eseguita dopo attenta <b>pianificazione radiologica </b>
e <b>clinica</b> e con il supporto delle migliori attrezzature medicali. Gli interventi 
vengono eseguiti in ambienti dedicati e in perfetta <b>sterilità</b> con personale altamente qualificato. 
Il posizionamento della corona o protesi ancorate all’impianto può avvenire a 
distanza di alcuni mesi dall’intervento (impianto a carico differito) o il giorno stesso 
(impianto a carico immediato). Attraverso l’ausilio di questa tecnica, il paziente 
<b>non rimane mai senza denti</b>, favorendone così l’estetica e il benessere psicologico. 



 Si eseguono: 
 * Impianti a carico differito 
 * Impianti a carico immediato
 * Riabilitazione totale fissa su impianti: All on 4, All on 6 
 * Overdenture su impianti  


<br> 
   <figure style="margin-bottom:0px; margin-right:50px; margin-left:10px; float:LEFT; width:50%"/>
  <img src="../../../img/Servizi/Implantologia2.png" style="margin-bottom:20px; width:100%"/>
</figure> 
 

 <h3> CHIRURGIA ORALE </h3>

Gli interventi di chirurgia orale presso Medical Murgia vengono eseguiti in una <b>sala chirurgica dedicata</b>, 
questo ci consente di mantenere gli standard ottimali di <b>sterilità</b>. Tutti i percorsi chirurgici sono supportati 
da strumenti radiodiagnostici di precisione e da <b>tecnologie all’avanguardia</b>.

 Si eseguono: 
* 	Estrazioni dentali (semplici e complesse)  
* 	Rimozione epulidi 
* 	Piccolo e grande rialzo di seno mascellare 
* 	Chirurgia pre protesica 
* 	Rimozione cisti 
* 	Ricostruzioni e innesti ossei 
* 	Apicectomie 
* 	Gengivectomie  
