+++
title = "Medicina Specialistica"
id = "Odontoiatira"
date = ""
mail = "marco.murgia@medicalmurgia.it"
tags = [""]
categories = [""]
banner = "img/banners/banner-4.jpg"
author = "Marco Murgia"
immagine = "img/Servizi/Odontoiatria.png"
blog ="N"

+++

# Odontoiatria 


Medical Murgia ti offre le migliori competenze in campo odontoiatrico.<br>
Il nostro staff, formato da professionisti affermati e specialisti nel proprio settore, a disposizione di tutta la 
famiglia per garantire l'accesso a un'<b>assistenza odontoiatrica completa e di alta qualità </b>, dalla 
visita di controllo fino agli interventi di implantologia e chirurgia.<br>
Tutte le prestazioni che proponiamo vengono eseguite presso la nostra sede e 
dalla stessa equipe formata da Odontoiatra, Endodontista, Medico Chirurgo, Ortodontista e Igienista.  <br>
Nel nostro Istituto potrai anche effettuare <b>radiografie</b> endorali e <b>panoramica</b> in sede, nonchè usufruire 
della <b>sedazione cosciente</b> per gli interventi chirurgici.  




 <div class="col-md-4">
              <a href="/medical-murgia/servizi/odonto/primavisita"><img  class="zoom" src="../../img/Odontoiatria/PrimaVisita.png" style="margin-bottom:20px; width:100%"/></a>
         <figcaption align="middle">Prima Visita</figcaption>
 </div>
 <div class="col-md-4">
   <div class=" customers ">      
            <a href="/medical-murgia/servizi/odonto/igiene"><img class="zoom" src="../../img/Odontoiatria/igiene.png" style="margin-bottom:20px; width:100%"/></a>
         <figcaption align="middle">Igiene orale e Parodontologia </figcaption>
    </div>
 </div>
  <div class="col-md-4">
   <div class=" customers ">      
            <a href="/medical-murgia/servizi/odonto/implantologia"><img class="zoom" src="../../img/Odontoiatria/Implantologia.png" style="margin-bottom:20px; width:100%"/></a>
         <figcaption align="middle">Implantologia e Chirurgia orale</figcaption>
     </div>
 </div>
  
 
 
 
 <div class="col-md-4">
   <div class=" customers ">      
           <a href="/medical-murgia/servizi/odonto/protesi"><img class="zoom" src="../../img/Odontoiatria/protesi.png" style="margin-bottom:20px; width:100%"/></a>
         <figcaption align="middle">Protesi</figcaption>
     </div>
 </div> 
 <div class="col-md-4">
   <div class=" customers ">      
            <a href="/medical-murgia/servizi/odonto/bimbi"><img class="zoom" src="../../img/Odontoiatria/Bambini.png" style="margin-bottom:20px; width:100%"/></a>
         <figcaption align="middle">Odontoiatria infantile</figcaption>
    </div>
 </div>
  <div class="col-md-4">
   <div class=" customers ">      
            <a href="/medical-murgia/servizi/odonto/invisalign"><img class="zoom" src="../../img/Odontoiatria/Ortodonzia.png" style="margin-bottom:20px; width:100%"/></a>
         <figcaption align="middle">Ortodonzia-Invisalign&reg;</figcaption>
    </div>
 </div>
 
 
 <br>
 
<h2>LA TECNOLOGIA E I DISPOSITIVI AL VOSTRO SERVIZIO </h2>
 <h3>STERILIZZAZIONE</h3>
 
I pazienti in cura e gli operatori sanitari hanno il diritto e l'obbligo di non essere esposti a 
nessun rischio di contagio o trasmissione microbica (infezioni crociate), 
per questo motivo la tutela della tua e della nostra salute è disciplinata da rigidi 
<b>protocolli di decontaminazione, sterilizzazione e sanificazione</b>.  

Tutti gli ambienti vengono accuratamente puliti e disinfettati, 
si utilizzano apposite <b>pellicole monouso</b> a protezione di tutte 
le superfici dei riuniti e vengono utilizzati tutti i <b>DPI</b> 
(Dispositivi di Protezione Individuale) previsti dalla legge. 
<br>

<h4>IL PROCESSO DI STERILIZZAZIONE DEGLI STRUMENTI </h4>

Gli strumenti non monouso seguono un preciso percorso obbligatorio che si compone delle seguenti fasi: 

1. <b>DECONTAMINAZONE</b>: gli strumenti vengono messi dall'assistente in una <b>vasca ad ultrasuoni</b> 
che eseguirà  automaticamente alcune fasi (disinfezione chimica, sciacquo, detersione in ultrasuoni,
sciacquo e asciugatura) prima dell'imbustamento. 

2. <b>IMBUSTAMENTO</b>: gli strumenti odontoiatrici vengono confezionati in buste per la sterilizzazione 
in carta/pellicola conforme. Le singole buste vengono poi sigillate ed etichettate 
(con data della sterilizzazione, data di scadenza e n° di lotto). 
In questo modo si ottiene la <b>tracciabilità </b> di ogni singolo strumento. 

3. <b>STERILIZZAZIONE</b>: l'assistente inserisce le buste con gli strumenti nell'<b>autoclave</b> 
che esegue una perfetta sterilizzazione degli strumenti, comprese le parti interne e non smontabili. 
Dopo il ciclo di sterilizzazione si compilano i moduli relativi ad esso che vengono 
archiviati nel <b>registro di sterilità </b>. 
L'autoclave viene inoltre sottoposta a diversi test (giornalieri, settimanali e mensili) 
che ne comprovino il perfetto funzionamento, 
nonchè periodicamente ad interventi di manutenzione ordinaria. 

 <h3>LASER A NEODIMIO YAG</h3> 
E' uno strumento dai tanti benefici, tra cui:  
	Minore invasività  delle tecniche operatorie 
	Assenza di dolore 
	Assenza di rumore e vibrazioni 
	Rispetto dei tessuti (Biocompatibilità ) 
	Rapida riparazione dei tessuti trattati 
	Decontaminazione da batteri 
	Biostimolazione 
 
 
 <h3>PIEZOSURGERY</h3> 

La chirurgia piezoelettrica è una metodica chirurgica che consente di intervenire sui tessuti 
ossei in maniera <b>atraumatica</b>. 
Questo perchè consente la massima precisione e, di conseguenza, la minima invasività . 
I vantaggi della chirurgia piezoelettrica sono: 
 * Ridotto sanguinamento intraoperatorio 
 * Favorevole guarigione tissutale 
 * Minor dolore e /o gonfiore post-operatorio 

 

<h3>RADIOLOGIA DIGITALE</h3>  
 
  <figure style="margin-bottom:0px; margin-right:20px; margin-left:0px; float:right; width:50%"/>
  <img src="../../img/Odontoiatria/Odonto1.png" style="margin-bottom:20px; width:100%"/>
</figure> 


La radiologia digitale ai fosfori presenta i seguenti vantaggi rispetto alla tecnica tradizionale: 
 * Alta qualità  delle immagini radiografiche 
 * Dose di esposizione radiogena ridotta 
 * Possibilità  di visualizzare le lastre al computer 

 

<h3>VIDEOCAMERA ENDORALE</h3>  

E' una piccola videocamera a luce led che consente al paziente di visualizzare direttamente 
sul <b>monitor</b> del riunito l'interno della sua bocca. Può essere utilizzato sia nel momento 
della prima visita, sia per far visualizzare chiaramente al paziente le differenze pre e post cure. 

 

<h3>SISTEMA RIGENERA con RIGENERACONS</h3>   

La tecnologia RIGENERA consente di intervenire sui tessuti danneggiati tramite un dispositivo 
monouso chiamato RICENERACONS. Si tratta di una capsula sterile in cui vengono posti frammenti 
di tessuto del paziente creando dei micro-innesti. 
I <b>micro-innesti</b> vengono poi utilizzati intra operativamente dal chirurgo per rigenerare 
le aree danneggiate. 

 
<h3>SEDAZIONE COSCIENTE</h3>  
 
La sedazione cosciente è una procedura terapeutica che si realizza mediante la somministrazione di farmaci attivi 
sul sistema nervoso centrale. Questa fornisce una condizione di rilassamento senza che il paziente perda 
i riflessi protettivi e la capacità  di interazione con il personale medico. 

Le procedure sedative vengono effettuate da personale altamente specializzato in manovre di 
sedazione cosciente e monitoraggio dei parametri vitali del paziente. 
La sedazione cosciente può servire a tutti: 

 * Al fobico 
 * Al bambino 
 * Al portatore di handicap 
 * Al paziente a rischio 

 

<h3>FOTOGRAFIA DIGITALE</h3>  

La fotocamera digitale viene utilizzata su tutti quei casi che necessitano una documentazione fotografica 
come i casi di terapia ortodontica. Questa tecnologia ci permette di documentare tramite le immagini 
la situazione di partenza, gli importanti passaggi terapeutici ed il risultato raggiunto. 

 

<h3>SPETTROFOTOMETRO</h3>  
 <figure style="margin-bottom:0px; margin-right:20px; margin-left:0px; float:left; width:50%"/>
  <img src="../../img/Odontoiatria/Odonto2.png" style="margin-bottom:20px; width:100%"/>
</figure> 

Questo strumento permette la rilevazione del colore dei denti in modo personalizzato e computerizzato. 
In questo modo è possibile creare manufatti protesici della tinta naturale dei denti
del paziente superando l'ostacolo della soggettività  del metodo tradizionale. 
Esso è infatti in grado di fornire all'odontotecnico una mappatura colorimetrica personalizzata 
che assicura la perfezione nel risultato finale.  

 

 