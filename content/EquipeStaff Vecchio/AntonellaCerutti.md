+++ 
tipo = "Curriculum"
title = "Pediatra"
id = "Pediatra"
date = ""
mail = "Antonella.Cerutti@medicalmurgia.it"
tags = [""]
categories = [""]
banner = "img/banners/banner-4.jpg"
author = "Dott.ssa ANTONELLA CERUTTI"
immagine = "img/Staff_Ext_MM/Medico2.png"
titolo1 = "Dott.ssa ANTONELLA CERUTTI"
subtitolo = "Specialista in Pediatria"
telefono = "Tel. 011 331794"
+++

<h4> Breve CV </h4>


<b>Formazione</b>


Laurea in Medicina e Chirurgia conseguita presso l’Università di Torino il 26/06/1974. 
Iscritta all’albo professionale dell’Ordine dei Medici della Provincia di Torino da 14/05/1975 (n°8733). 
Medico Interno con mansioni assistenziali presso la Clinica Pediatrica I dell’Università di Torino e Tirocinio 
pratico ospedaliero nella disciplina di Pediatria presso l’Ospedale Infantile Regina Margherita di Torino. 
Diploma di Specialista in Clinica Pediatrica conseguito il 20/07/1977. 
 

<b>Collaborazioni professionali</b>

Supplenza temporanea al posto di assistente di Pediatria presso il reparto delle Malattie Infettive dell’Ospedale 
Regina Margherita di Torino nel periodo dal 01/01/1978 al 01/01/1979. Medico generico presso mutua artigiani e commercianti. 
Pediatra di libera scelta presso ASL TO1 di Torino nel periodo da ottobre 1977 a dicembre 2017. 

<b>Attività  attuali</b>

Da gennaio 2018 ad oggi Pediatra Libero Professionista 


<b>Pubblicazioni</b>

* Disgenesie cerebrali mediane dell’infanzia 
* Studio su alcuni parametri immunologici in corso di salmonellosi minore in soggetti prematuri e lattanti 
* Pierre Robin syndrome with hyperphalangism – clinodactylysm of the Index Finger: a possible New Palate-digital Syndrome (??? Non era puntato ma inserito nel punto elenco precedente, a me sembra una roba del tutto diversa ??!?!) 
* L’angioma cavernoso del fegato nella prima infanzia 
* Studio del rapporto tra sideremia e sintesi dell’Hb A2 nel lattante 


<b>Affiliazioni</b> 

Iscritta fin dal 1977 a 