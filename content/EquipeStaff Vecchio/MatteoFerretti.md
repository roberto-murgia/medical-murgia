+++ 
tipo = "Curriculum"
title = "Matteo Ferretti"
id = "Ortopedico"
date = ""
mail = "matteo.ferretti@medicalmurgia.it"
tags = [""]
categories = [""]
banner = "img/banners/banner-4.jpg"
author = "Matteo Ferretti"
immagine = "img/Staff_Ext_MM/MFerretti.jpg"
titolo1 = "Dott. MATTEO FERRETTI"
subtitolo = "Specialista in Ortopedia"
telefono = "Tel. 011 331794"
+++


<h4> Breve CV </h4>


<b>Formazione</b>

Specialista in Ortopedia e Traumatologia presso Università di Roma "Sapienza" II, Facoltà di Medicina e Chirurgia, Policlinico S. Andrea, U.O. di Ortopedia e Traumatologia, Direttore: Prof. A Ferretti, il 03/11/2009
Laurea in Medicina e Chirurgia ottenuta presso l'Università di Torino il 17/11/2003
Iscritto all'Ordine di Medici di Roma
Partecipazione ai corsi: PHTLS, ATLS, AMLS


<b>Collaborazioni professionali</b>

Medico frequentatore presso reparto di Ortopedia e Traumatologia Ospedale Mauriziano di Torino nel periodo 11/2003-05/2004
Medico presso reparto di Ortopedia e Traumatologia Clinica Villa Aurora -Roma - nel periodo 09/2004-05/2005
Arthroplasty fellowship presso Hairmyres Hospital, East Kilbride, United Kingdom, nel periodo 12/2008 - 07/2009

Ortopedico presso reparto di Ortopedia e Traumatologia Clinica Villa Aurora -Roma - nel periodo 11/2009-05/2012
Dirigente medico presso reparto di Ortopedia e Traumatologia Ospedale di Albano Laziale ASL Roma H con contratto libero professionale di sostituzione nel periodo 06-08/2012
Ortopedico presso Clinica Mater Dei -Roma - nel periodo 09/2012-03/2013
Dirigente medico presso reparto di Ortopedia e Traumatologia Ospedale di Anzio ASL Roma H con contratto libero professionale di sostituzione nel periodo 04-05/2013
Dirigente medico in Ortopedia e Traumatologia presso UOC di Ortopedia e Traumatologia Ospedale Belcolle, AUSL Viterbo, dal 08/07/2013 al 12/06/15
Dirigente medico a tempo indeterminato presso reparto di Ortopedia e Traumatologia Ospedale di Rivoli (TO)  ASL Torino3, dal 16/11/15 al 28/02/18


Medico della Squadra Nazionale Pre-Juniores di Pallavolo Femminile nel periodo 12/2005- 08/2007
Medico della Squadra Nazionale di Ginnastica Artistica Maschile e Femminile (FGI) dal 2006
Ortopedico nello staff medico del Pharaons Rally 


<b>Attività attuali</b>

Ortopedico presso Clinica Cellini, Torino
Medico della Squadra Nazionale di Ginnastica Artistica Maschile e Femminile
Ortopedico dello staff medico del Pharaons Rally


<b>Pubblicazioni</b>

*	A lateral minimal-incision technicque in total hip replacement: a prospective, randomizes, controlled trial - A. Speranza, R. Iorio, M. Ferretti, C. D'Arrigo, A. Ferretti - Hip International 2007; 17: 4 - 8
*	Tendinopatia calcifica della spalla con erosione ossea:caso clinico con riassorbimento del trochine - De Carli A. ,Di Vavo I., Ferretti M., Noto S., Ferretti A. - Agg. CIO 2007, 13:26-30
*	The gmnast's shoulder: MRI and clinical findings - De Carli A, Mossa L, Larciprete M, Ferretti M, Argento G, Ferretti A. - J Sports Med Phys Fitness. 2012 Feb;52(1):71-9.
*	Platelet-rich plasma: does it help reduce tunnel widening after ACL reconstruction? - Vadalà A, Iorio R, De Carli A, Ferretti M, Paravani D, Caperna L, Iorio C, Gatti A, Ferretti A. - Knee Surg Sports Traumatol Arthrosc. 2012 Apr 10. 
*	Patellar tendon ossification after anterior cruciate ligament reconstruction using bone-patellar tendon- bone autograft - Camillieri G, Di Sanzo V, Ferretti M, Calderaro C, Calvisi V.-  BMC Musculoskelet Disord. 2013 May 10;14(1):164. 
*	Intra-articular tenosynovial giant cell tumor arising from the posterior cruciate ligament - Camillieri G, Di Sanzo V, Ferretti M, Calderaro C, Calvisi V. - Orthopedics. 2012 Jul 1;35(7):e1116-8. 


<b>Affiliazioni</b> 

Socio ESSKA (European Society for Sports Traumatology, Knee Surgery and Arthroscopy)
Socio EKA (European Knee Associates)

<b>Comunicazioni a congressi come relatore</b> 

*	"Test a carico ciclico su differenti complessi di fissazone nella sutura della cuffia dei rotatori". Ferretti M., Monaco E., Vadalà A., Labianca L., De Carli A., Ferretti A. 96° congresso SOTIMI, Cagliari, luglio 2005
*	"Ruolo della plastica periferica nella ricostruzione del LCA". Ferretti M., Monaco E., Labianca L., De Carli A., Ferretti A. 11° congresso nazionale specializzandi in ortopedia e traumatologia, Parma, febbraio 2006
*	"Ruolo della plastica periferica nella ricostruzione del LCA". Ferretti M., Monaco E., Labianca L., De Carli A., Ferretti A. The rehabilitation of winter and mountain sport incurie, Torino, Aprile 2006
*	"Ricostruzione del LCA con tecnica a doppio fascio: valutazione biomeccanica in vivo con navigatore". Ferretti M., Monaco E., Labianca L. 12° congresso nazionale specializzandi in ortopedia e traumatologia, Parma, febbraio 2007
*	"Ricostruzione del LCA con tecnica a doppio fascio: valutazione biomeccanica in vivo con navigatore". Ferretti M., Monaco E., Labianca L. 17° congresso SIBOT, Roma, novembre 2007
*	"The role of platelet rich plasma in bone graft integration after ACL reconstruction", Ferretti M., De Carli A., Ferretti A., XIX International Congress of Sports REhabilitation and Traumatology, Bologna, Aprile 2010
*	"PRP: does it help reducing tunnel widening after ACL reconstruction? A CT study.", Ferretti M., Vadalà A., De Carli A., Ferretti A., AAOS annual meeting, San Diego, 2011

<b>Corsi</b>

*	II corso nazionale SICSeG, Bologna, dicembre 2005
*	Corso teorico-pratico lesioni della cuffia dei rotatori". Ospedale Sant' Andrea Roma, Febbraio 2008
*	Corso teorico-pratico "Artroprotesi d'anca", Ospedale S. Andrea, Roma, Aprile 2008
*	Corso teorico-pratico "Artroprotesi di ginocchio", Ospedale S. Andrea, Roma, Aprile 2008
*	Corso teorico-pratico "La fissazione interna ed esterna delle fratture". Educational center Orthofix, Verona, giugno 2008
*	Oxford knee training day - Biomet -, Airth, marzo 2009
*	SIA Scuola di artroscopia 1° corso: spalla e ginocchio, Arezzo, Dicembre 2010
*	SIA Scuola di artroscopia Advanced Course: spalla, Arezzo, giugno 2011
*	LINKademy International symposium on Hip Joint Arthrolasty, Hamburg, luglio 2011 


<b>Referenze</b>

Prof. A. Ferretti, Direttore UOC di Ortopedia e Traumatologia Ospedale 
S. Andrea, Università di Roma "Sapienza" II <br>
Mr A Gregori, Deputy Clinical Director, Orthopaedics, Hairmyres Hospital, East Kilbride, United Kingdom<br>
Dr. M. Forti, responsabile Chirurgia del Ginocchio Clinica Humanitas Cellini, Torino

